(defun rl-skeletor-substitutions-init()
  (add-to-list 'skeletor-global-substitutions
               (cons "__QUARTUS-TIME__" (lambda () (format-time-string "%H:%M:%S  %B %e, %Y"))))
  (add-to-list 'skeletor-global-substitutions
               '("__QUARTUS-VERSION__" . "18.1"))
)

(defun rl-skeletor-templates-init()
  (skeletor-define-template "rl-cpp"
    :title "|C++ Project")
  (skeletor-define-template "rl-altera-fpga"
    :title "|FPGA Altera")
)

(defun rl-skeletor-init()
  (setq skeletor-user-directory "~/.emacs.d/private/skeletor/templates")
  (rl-skeletor-substitutions-init)
  (rl-skeletor-templates-init)
)

(provide 'rl-skeletor)

(require 'rl-menu)

(defun rl-execute-org-src-block()
  (interactive)
  (message "here")
  (call-interactively 'org-babel-execute-src-block)
  )

(setq rl-menu-mhashm-org-mode
      #s(hash-table
         test equal
         data (
               "Execute Source" rl-execute-org-src-block
              )
         )
      )

(provide 'rl-menu-org)



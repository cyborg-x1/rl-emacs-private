(defun rl-cmake-ide-settings()
  (setq-default dotspacemacs-configuration-layers
                '((c-c++ :variables
                         c-c++-default-mode-for-headers 'c++-mode)))
  (setq-default dotspacemacs-configuration-layers
                '((c-c++ :variables c-c++-enable-clang-support t)))
  ;; Bind clang-format-region to C-M-tab in all modes:
  (global-set-key [C-M-tab] 'clang-format-region)
  ;; Bind clang-format-buffer to tab on the c++-mode only:
  (add-hook 'c++-mode-hook 'clang-format-bindings)
  (defun clang-format-bindings ()
    (define-key c++-mode-map [tab] 'clang-format-buffer))

  (eval-after-load 'company
    '(add-to-list 'company-backends 'company-irony))

  (setq-default dotspacemacs-configuration-layers
                '((auto-completion :variables
                                   auto-completion-enable-snippets-in-popup t)))
)

(defun rl-cmake-compile ()
  "Run cmake and compile after"
  (interactive)
  (cmake-ide-run-cmake)
  (sleep-for 1)
  (cmake-ide-compile)
  )

(defun rl-project_exe()
  "run my shit"
  (interactive)
  (setq projdir (locate-dominating-file (buffer-file-name) ".dir-locals.el"))
  (shell-command (concat projdir exe))
  )

(defun rl-register-std-cpp-style()
  (c-add-style "rl-std-cpp-style"
               '("bsd"
                 (c-offsets-alist . ((inline-open . 0)
                                     (brace-list-open . 0)
                                     (statement-case-open . +)
                                     )
                                  )
                 (c-basic-offset . 4)            ; four spaces indentation
                 (indent-tabs-mode . nil)        ; spaces 
                 )

               )
  )

(defun rl-cmake-ide-init()
  "Init c++ stuff"
  (rl-cmake-ide-settings)
  (rl-register-std-cpp-style)
)

(provide 'rl-cmake-ide)

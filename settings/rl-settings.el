
(defun rl-startup()
  (require 'neotree)
  (require 'tabbar)
  (require 'yasnippet)

  (setq neo-window-fixed-size nil)
  ;;(neotree-show)
  (treemacs)
  (tabbar-mode)
  (yas-exit-all-snippets)
  (setq yas-snippet-dirs '("~/.emacs.d/private/snippets")) ;;set snippet dir
  (yas-reload-all)
  )

(defun rl-apply-settings()
  ;;
  (require 'rl-menu-neotree)
  (require 'rl-menu-org)

  ;; Skeletor init
  (add-to-list 'load-path' "~/.emacs.d/private/skeletor")
  (require 'rl-skeletor)
  (rl-skeletor-init)
  ;;;

  ;;; emacs as server
  (server-start)
  ;;;

  ;;; file versioning
  (setq version-control t)
  (setq kept-new-versions 100)
  (setq kept-old-versions 100)
  (setq backup-by-copying t)
  (setq backup-directory-alist `(("." . "~/.emacs.d/private/.saves")))
  ;;;

  ;;; copy paste
  (cua-mode t)
  ;;;

  ;;; recent files
  (recentf-mode 1)
  (setq recentf-max-menu-items 100)
  (setq recentf-max-saved-items 100)
  ;;;

  ;;; Display CFS on case fold search
  (add-to-list 'minor-mode-alist '(case-fold-search " CFS "))
  ;;

  (require 'rl-keys)
  (rl-keys-init)

  (require 'rl-cmake-ide)
  (rl-cmake-ide-init)

  (rl-startup)
)





(provide 'rl-settings)


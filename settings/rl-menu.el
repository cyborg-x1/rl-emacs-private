(require 'popup)
(require 'menu-bar)
(require 'rl-general)
(require 'subr-x)





(defun rl-menu-submenu-file()
  "launches rl submenu file"
  (interactive)
  (let (
        (choice (popup-menu* '("-"
                               "New File"
                               "Close File"
                               "Recent Files"
                               "Save File as"
                               )))
        )
    (if (string= choice  "New File") (rl-new-empty-buffer))
    (if (string= choice  "Close File") (call-interactively 'kill-this-buffer))
    (if (string= choice  "Recent Files") (call-interactively 'recentf-open-files))
    (if (string= choice  "Save File as") (call-interactively 'write-file))
    )
  )


(defun rl-menu-submenu-code()
  "launches rl submenu code"
  (interactive)
  (let (
        (choice (popup-menu* '("-"
                               "comment"
                               "uncomment"
                               "auto-indent-remove-whitespace"
                               )))
        )
    (if (string= choice  "comment") (call-interactively 'comment-region))
    (if (string= choice  "uncomment") (call-interactively 'uncomment-region))
    (if (string= choice  "auto-indent-remove-whitespace") (call-interactively 'rl-iwb))

    )
  )


(defun rl-menu-submenu-search()
  "launches search menu"
  (interactive)
  (let (
        (choice (popup-menu* '("-"
                               "Toggle Case Sensitivity"
                               "Replace Regex"
                               "Replace String"
                               "Search Forward"
                               "Search Backward"
                               )))
        )
    (if (string= choice  "Toggle Case Sensitivity")
      (progn
      (call-interactively 'toggle-case-fold-search)
      (call-interactively 'rl-menu-submenu-search)
      )
    )
    (if (string= choice  "Replace Regex") (call-interactively 'replace-regexp))
    (if (string= choice  "Replace String") (call-interactively 'replace-string))
    (if (string= choice  "Search Forward") (call-interactively 'isearch-forward))
    (if (string= choice  "Search Backward") (call-interactively 'isearch-backward))
    )
)

(defun rl-eval-string (string)
  (eval (car (read-from-string (format "(progn %s)" string)))))


(defun rl-menu-from-hashmap (entrymap)
  (interactive)
  (let (
        (menu-entries (list))
        (func)
       )
    (setq menu-entries (append menu-entries (hash-table-keys entrymap) (list "-")))
    (setq menu-entries (reverse menu-entries))
    (popup-menu* menu-entries)
  )
)

(setq rl-std-menu-hashmap
      #s(hash-table
         test equal
         data (
               "Code" rl-menu-submenu-code
               "Search Replace" rl-menu-submenu-search
               "Autocomplete" auto-complete
               "Yasnippet" yas-insert-snippet
               "imenu" imenu
               "File" rl-menu-submenu-file
               )))

;; for debugging (makunbound 'rl-menu-mhashm-emacs-lisp-mode)
(defun rl-menu()
  (interactive)

  (let (
        (mhashmap-name (concat "rl-menu-mhashm-" (prin1-to-string major-mode)))
        (mhashmap)
        (selection)
        )
    (print (concat "showing menu for " mhashmap-name))
    (rl-eval-string (concat "(defvar " mhashmap-name " rl-std-menu-hashmap)")) ;; create variable if not exist
    (rl-eval-string (concat "(setq mhashmap " mhashmap-name ")")) ;; init mhashmap
    (rl-eval-string (concat "(setq selection (rl-menu-from-hashmap " mhashmap-name "))"))

    (if (not (string= selection "-"))
        (call-interactively (gethash selection mhashmap))
    )
  )
)

(provide 'rl-menu)
